import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { RxService } from '../rx.service';

@Component({
  selector: 'app-rx-tree',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-block [item]="data"></app-block>
  `,
  styleUrls: ['./rx-tree.component.css']
})
export class RxTreeComponent implements OnInit {
  data: { name: String };

  constructor(public rxService: RxService, private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.rxService.data$.subscribe(value => {
      this.data = value;
      this.cd.detectChanges();
    });
  }

}
