import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RxTreeComponent } from './rx-tree.component';

describe('RxTreeComponent', () => {
  let component: RxTreeComponent;
  let fixture: ComponentFixture<RxTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RxTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RxTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
