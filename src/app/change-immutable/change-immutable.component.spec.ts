import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftImmutableComponent } from './left-immutable.component';

describe('LeftImmutableComponent', () => {
  let component: LeftImmutableComponent;
  let fixture: ComponentFixture<LeftImmutableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftImmutableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftImmutableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
