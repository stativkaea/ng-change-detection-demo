import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-change-immutable',
  template: `
    <button (click)="change('left')">Change LEFT immutably</button>
    <button (click)="change('right')">Change RIGHT immutably</button>
  `,
  styleUrls: ['./change-immutable.component.css']
})
export class ChangeImmutableComponent implements OnInit {
  @Output() changeImmutably = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  change(side: string) {
    this.changeImmutably.emit(side);
  }
}
