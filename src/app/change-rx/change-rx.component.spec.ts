import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeRxComponent } from './change-rx.component';

describe('ChangeRxComponent', () => {
  let component: ChangeRxComponent;
  let fixture: ComponentFixture<ChangeRxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeRxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeRxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
