import { Component, OnInit } from '@angular/core';
import { RxService } from '../rx.service';

@Component({
  selector: 'app-change-rx',
  template: `
    <button (click)="trigger('left')">Mutate LEFT by RX</button>
    <button (click)="trigger('right')">Mutate RIGHT by RX</button>
    <button (click)="changeData()">Change RX value</button>
  `,
  styleUrls: ['./change-rx.component.css']
})
export class ChangeRxComponent implements OnInit {

  constructor(private rxService: RxService) { }

  ngOnInit() {
  }

  trigger(side: 'left' | 'right') {
    this.rxService.updateRx(side);
  }

  changeData() {
    this.rxService.updateData();
  }

}
