import {
  Component,
  AfterViewChecked,
  Input,
  Output,
  ElementRef,
  Renderer2,
  ViewChild,
  EventEmitter,
  ChangeDetectorRef
} from '@angular/core';

@Component({
  selector: 'app-block',
  template: `
    <div #block class="block" (click)="fireEvent()">
      {{item.name}}
    </div>
  `,
  styles: [`
    .block {
      border: 2px solid black;
      border-radius: 3px;
      background: #222;
      box-shadow: 0 0 18px 0 rgba(0,0,0,0.75);
      margin: 20px;
      padding: 15px 25px;
      color: #ccc;
      transition: all 1s;
      cursor: pointer;
    }

    .highlighted {
      box-shadow: 0 0 24px 4px rgba(0,0,0,0.75);
      transform: scale(1.2);
      color: white;
      background: #EF6C00;
    }

    .event-trigger {
      background: #D50000;
    }
  `]
})
export class BlockComponent implements AfterViewChecked {
  @Input() item: { name: String };
  @ViewChild('block') el: ElementRef;
  @Output() event = new EventEmitter<String>();

  constructor(private rd: Renderer2) { }

  ngAfterViewChecked() {
    this.rd.addClass(this.el.nativeElement, 'highlighted');
  }

  public fireEvent() {
    this.event.emit('event');
    this.rd.addClass(this.el.nativeElement, 'event-trigger');
  }
}
