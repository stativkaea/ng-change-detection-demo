import { Component, OnInit, Input } from '@angular/core';
import { RxService } from '../rx.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-rx',
  templateUrl: './rx.component.html',
  styleUrls: ['./rx.component.css']
})
export class RxComponent implements OnInit {
  @Input() data: Observable<Object>;

  _data: Object;

  constructor(private rxService: RxService) { }

  ngOnInit() {
    this.rxService.data$.subscribe(value => {
      this._data = value;
    });
  }

}
