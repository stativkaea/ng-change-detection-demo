export interface Block {
  name: string;
  children?: Block[];
  rx?: Boolean;
}

export let left: Block[] = [
  {
    name: 'LEFT',
    children: [
      {
        name: 'A',
        children: [
          {
            name: 'E'
          },
          {
            name: 'F',
            rx: true
          }
        ]
      },
      {
        name: 'B',
        children: [
          {
            name: 'G'
          },
          {
            name: 'H'
          }
        ]
      }
    ]
  },
];

export let right: Block[] = [
  {
    name: 'RIGHT',
    children: [
      {
        name: 'C',
        children: [
          {
            name: 'I'
          },
          {
            name: 'K'
          }
        ]
      },
      {
        name: 'D',
        children: [
          {
            name: 'L'
          },
          {
            name: 'M'
          }
        ]
      }
    ]
  }
];
