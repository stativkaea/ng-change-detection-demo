import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { RxService } from './rx.service';
import { left, right } from './data';

@Component({
  selector: 'app-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="wrapper">
      <h1>{{title}}</h1>
      <app-block [item]="{ name: 'root' }"></app-block>
      <div class="trees">
        <app-smart-tree [data]="_left"></app-smart-tree>
        <app-smart-tree [data]="_right"></app-smart-tree>
        <app-rx-tree></app-rx-tree>
      </div>
      <app-event-trigger></app-event-trigger>
      <app-change-mutable (changeMutably)="onMutableChange($event)"></app-change-mutable>
      <app-change-immutable (changeImmutably)="onImmutableChange($event)"></app-change-immutable>
      <app-change-rx></app-change-rx>
    </div>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Change detection demo';
  _left = left;

  _right = right;

  constructor(private rxService: RxService, private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.rxService.left$.subscribe(value => {
      this._left = value;
      console.log(value);
      this.cd.markForCheck();
    });
    this.rxService.right$.subscribe(value => {
      this._right = value;
      this.cd.markForCheck();
    });
  }

  onImmutableChange(side: string) {
    switch (side) {
      case 'left':
        this._left = [...this._left];
        break;

      case 'right':
        this._right = [...this._right];
        break;
    }
  }

  onMutableChange(side) {
    switch (side) {
      case 'left':
        this._left[0].name = 'MUTATED';
        break;

      case 'right':
        this._right[0].name = 'MUTATED';
        break;
    }
  }
}
