import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { left, right } from './data';

@Injectable()
export class RxService {
  public left$ = new BehaviorSubject(left);
  public right$ = new BehaviorSubject(right);

  public data$ = new BehaviorSubject({ name: 'RX initial value' });

  constructor() { }

  public updateRx(side: 'left' | 'right') {
    this[`_${side}`][0].children[0].children[0].name = 'updated by RX';
    this[`${side}$`].next(this[`_${side}`]);
  }

  public updateData() {
    this.data$.next({ name: 'RX updated value' });
  }
}
