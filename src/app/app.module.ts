import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TreeComponent } from './tree/tree.component';
import { EventTriggerComponent } from './event-trigger/event-trigger.component';
import { BlockComponent } from './block/block.component';
import { SmartTreeComponent } from './smart-tree/smart-tree.component';
import { ChangeImmutableComponent } from './change-immutable/change-immutable.component';
import { ChangeMutableComponent } from './change-mutable/change-mutable.component';

import { RxService } from './rx.service';
import { ChangeRxComponent } from './change-rx/change-rx.component';
import { RxComponent } from './rx/rx.component';
import { RxTreeComponent } from './rx-tree/rx-tree.component';
import { CdTogglerComponent } from './cd-toggler/cd-toggler.component';

@NgModule({
  declarations: [
    AppComponent,
    TreeComponent,
    EventTriggerComponent,
    BlockComponent,
    SmartTreeComponent,
    ChangeImmutableComponent,
    ChangeMutableComponent,
    ChangeRxComponent,
    RxComponent,
    RxTreeComponent,
    CdTogglerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [RxService],
  bootstrap: [AppComponent]
})
export class AppModule { }
