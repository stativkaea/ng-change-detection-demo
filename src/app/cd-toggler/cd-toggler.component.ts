import { Component, OnInit, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-cd-toggler',
  template: `
    <button (click)="toggleCD()" [ngClass]="(_enabled ? 'green' : 'red')">
      Turn {{(_enabled ? 'off' : 'on')}} CD
    </button>
  `,
  styles: [`
    button {
      color: white;
    }

    .green {
      background-color: green;
    }

    .red {
      background-color: red;
    }
  `]
})
export class CdTogglerComponent implements OnInit {
  @Output() cd = new EventEmitter<boolean>();
  public _enabled = true;

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
  }

  public toggleCD() {
    this._enabled = !this._enabled;
    this.cd.emit(this._enabled);
    this.cdr.detectChanges();
  }
}
