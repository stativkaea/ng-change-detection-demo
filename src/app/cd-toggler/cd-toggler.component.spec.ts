import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdTogglerComponent } from './cd-toggler.component';

describe('CdTogglerComponent', () => {
  let component: CdTogglerComponent;
  let fixture: ComponentFixture<CdTogglerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdTogglerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdTogglerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
