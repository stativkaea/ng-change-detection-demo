import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-tree',
  template: `
    <ul>
      <li *ngFor="let item of data">
        <app-block [item]="item"></app-block>
        <app-tree *ngIf="item.children" [data]="item.children"></app-tree>
      </li>
    </ul>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.Default
})
export class TreeComponent {
  @Input() data: any;

  constructor() { }

}
