import { Component } from '@angular/core';

@Component({
  selector: 'app-event-trigger',
  template: `
    <button (click)="fireEvent()">FIRE EVENT</button>
  `,
  styleUrls: ['./event-trigger.component.css']
})
export class EventTriggerComponent {

  constructor() { }

  fireEvent() {}

}
