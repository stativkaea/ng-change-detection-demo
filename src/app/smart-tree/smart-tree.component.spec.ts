import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartTreeComponent } from './smart-tree.component';

describe('SmartTreeComponent', () => {
  let component: SmartTreeComponent;
  let fixture: ComponentFixture<SmartTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
