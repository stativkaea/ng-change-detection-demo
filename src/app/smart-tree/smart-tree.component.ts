import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-smart-tree',
  template: `
    <div class="tree">
      <app-cd-toggler (cd)="changeCD($event)"></app-cd-toggler>
      <ul>
        <li *ngFor="let item of data">
          <app-block [item]="item"></app-block>
          <app-smart-tree *ngIf="item.children" [data]="item.children"></app-smart-tree>
          <div *ngIf="item.rx">
            <app-rx-tree></app-rx-tree>
          </div>
        </li>
      </ul>
    </div>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.Default
})
export class SmartTreeComponent {
  @Input() data: any;

  constructor(private cd: ChangeDetectorRef) { }

  public changeCD(flag: boolean) {
    console.log(flag);

    if (flag) {
      this.cd.reattach();
    } else {
      this.cd.detach();
    }
  }
}
