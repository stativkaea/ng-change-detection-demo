import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-change-mutable',
  template: `
    <button (click)="change('left')">Mutate LEFT</button>
    <button (click)="change('right')">Mutate RIGHT</button>
  `,
  styleUrls: ['./change-mutable.component.css']
})
export class ChangeMutableComponent implements OnInit {
  @Output() changeMutably = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  change(side: string) {
    this.changeMutably.emit(side);
  }
}
