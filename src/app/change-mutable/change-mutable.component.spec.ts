import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeMutableComponent } from './change-mutable.component';

describe('ChangeMutableComponent', () => {
  let component: ChangeMutableComponent;
  let fixture: ComponentFixture<ChangeMutableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeMutableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeMutableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
