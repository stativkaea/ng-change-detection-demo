import { ChangeDetectionDemoPage } from './app.po';

describe('change-detection-demo App', () => {
  let page: ChangeDetectionDemoPage;

  beforeEach(() => {
    page = new ChangeDetectionDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
